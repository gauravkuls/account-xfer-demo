/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.ctlr;

import com.gauravk.acctxfer.ach.impl.AchTransferManagerImpl;
import com.gauravk.acctxfer.ach.impl.AchTransferManagerInterface;
import com.gauravk.acctxfer.gen.AcctTransferRequest;
import com.gauravk.acctxfer.gen.util.JacksonUtil;
import com.gauravk.acctxfer.gen.util.JacksonUtilInterface;

import spark.Request;
import spark.Response;
import spark.Route;

public class AchTransferCtlr implements Route{

	AchTransferManagerInterface achXferCtlr = AchTransferManagerImpl.getInstance();
	
	@Override
	public String handle(Request request, Response response) throws Exception {
		String body = request.body();
		JacksonUtilInterface jacksonUtil = JacksonUtil.getInstance();
		AcctTransferRequest achXferRequest=jacksonUtil.parseAch(body);
		String jsonRes = jacksonUtil.writeAch(achXferCtlr.doAchTransfer(achXferRequest));
		System.out.println(jsonRes);
		return jsonRes;
	}

}
