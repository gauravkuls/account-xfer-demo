/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */

package com.gauravk.acctxfer.ctlr;

import java.util.Map;

import com.gauravk.acctxfer.gen.AccountTransferNotificationSingle;
import com.gauravk.acctxfer.gen.AcctTransferNotificationRequest;
import com.gauravk.acctxfer.gen.AcctTransferNotificationResponse;
import com.gauravk.acctxfer.gen.AcctTransferRequest;
import com.gauravk.acctxfer.gen.cache.CacheManagerImpl;
import com.gauravk.acctxfer.gen.notifcn.NotifierInterface;
import com.gauravk.acctxfer.gen.notifcn.PhoneNotifier;
import com.gauravk.acctxfer.gen.util.JacksonUtil;
import com.gauravk.acctxfer.gen.util.JacksonUtilInterface;

import spark.Request;
import spark.Response;
import spark.Route;

public class AchTransferNotificationCtlr implements Route {

	@Override
	public String handle(Request request, Response response) throws Exception {
		String body = request.body();
		JacksonUtilInterface jacksonUtil = JacksonUtil.getInstance();
		AcctTransferNotificationRequest notificRequest = jacksonUtil.parseAchNotification(body);

		Map<String, AcctTransferRequest> acctInfo = CacheManagerImpl.getInstance().getAchInfoDetails();

		String jsonRes = null;
		for (AccountTransferNotificationSingle item : notificRequest.getNotifications()) {
			NotifierInterface phoneNotifier = PhoneNotifier.getInstance();

			AcctTransferRequest acctReq = acctInfo.get(item.getToken());

			if (acctReq != null) {
				phoneNotifier.notify(acctReq.getOrgntrPhoneNumber(), "Dear " + acctReq.getOriginName() + ", Status of your transaction with token Id :" + item.getToken() + " is :" + item.getTransferStatus() + " with message :" + item.getErrorMessage());
				phoneNotifier.notify(acctReq.getDestinPhoneNum(), "Dear " + acctReq.getDestinName() + ", Status of your transaction with token Id :" + item.getToken() + " is :" + item.getTransferStatus() + " with message :" + item.getErrorMessage());
			} else {
				System.out.println("Invalid token Id :"+item.getToken() +". Skipping");
			}
		}
		AcctTransferNotificationResponse notifiResponse = new AcctTransferNotificationResponse();
		notifiResponse.setAckStatus(AcctTransferNotificationResponse.ACK_STATUS.SUCCESS);
		jsonRes = jacksonUtil.writeAchNotification(notifiResponse);

		System.out.println(jsonRes);

		return jsonRes;
	}

}
