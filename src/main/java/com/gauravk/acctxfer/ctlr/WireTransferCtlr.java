/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.ctlr;

import com.gauravk.acctxfer.gen.util.JacksonUtil;
import com.gauravk.acctxfer.gen.util.JacksonUtilInterface;
import com.gauravk.acctxfer.wire.WireTransferRequest;
import  com.gauravk.acctxfer.wire.impl.WireTransferManagerImpl;
import com.gauravk.acctxfer.wire.impl.WireTransferManagerInterface;

import spark.Request;
import spark.Response;
import spark.Route;
public class WireTransferCtlr implements Route {

	WireTransferManagerInterface wireXferCtlr = WireTransferManagerImpl.getInstance();
	
	@Override
	public String handle(Request request, Response response) throws Exception {
		String body = request.body();
		JacksonUtilInterface jacksonUtil = JacksonUtil.getInstance();
		WireTransferRequest wireXferRequest=jacksonUtil.parseWire(body);
		String jsonRes = jacksonUtil.writeWire(wireXferCtlr.doWireTransfer(wireXferRequest));
		System.out.println(jsonRes);
		return jsonRes;
	}

}
