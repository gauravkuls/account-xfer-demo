/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen;

import java.math.BigDecimal;

public class Account {
	
	public static enum ACCOUNT_STATUS {ACTIVE, INACTIVE};
	private String routingNum;
	private String accountNum;
	private String iBanNum;
	
	private String holderName;
	private String address;
	private BigDecimal balance;
	private ACCOUNT_STATUS accountStatus;
	
	public String getRoutingNum() {
		return routingNum;
	}
	public void setRoutingNum(String routingNum) {
		this.routingNum = routingNum;
	}
	public String getAccountNum() {
		return accountNum;
	}
	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}
	public String getiBanNum() {
		return iBanNum;
	}
	public void setiBanNum(String iBanNum) {
		this.iBanNum = iBanNum;
	}
	public String getHolderName() {
		return holderName;
	}
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public ACCOUNT_STATUS getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(ACCOUNT_STATUS accountStatus) {
		this.accountStatus = accountStatus;
	}

}
