/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen.notifcn;

public interface NotifierInterface {

	public void notify(String destination, String message);
}
