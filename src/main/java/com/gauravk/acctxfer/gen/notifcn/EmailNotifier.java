/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen.notifcn;

public class EmailNotifier implements NotifierInterface {
	
	private static NotifierInterface instance;
	
	public static NotifierInterface getInstance() {
		
		if(instance==null) {
			synchronized(EmailNotifier.class) {
				if(instance==null) {
					instance = new EmailNotifier();
				}
			}
		}
		
		return instance;
	}

	@Override
	public void notify(String destination, String message) {

		System.out.println("Sent email message :" + message);

	}

}
