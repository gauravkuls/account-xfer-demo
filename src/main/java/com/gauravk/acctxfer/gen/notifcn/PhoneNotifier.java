/*
 * (C) Copyright 2020 Gaurav K
 * 
 */
		
package com.gauravk.acctxfer.gen.notifcn;

public class PhoneNotifier implements NotifierInterface {

	private static NotifierInterface instance;

	public static NotifierInterface getInstance() {

		if (instance == null) {
			synchronized (PhoneNotifier.class) {
				if (instance == null) {
					instance = new PhoneNotifier();
				}
			}
		}

		return instance;
	}

	@Override
	public void notify(String destination, String message) {

		System.out.println("Sent SMS message :" + message);

	}

}
