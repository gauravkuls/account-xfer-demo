/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen;

public interface TransferManagerInterface {
	public enum FIN_VALIDN_STATUS {
		SUCCESS, SRC_ACCT_INVALID, SRC_INSUFF_FUNDS, DESTN_ACCT_INVALID, SYSTEM_FAILURE_SRC_CHECK, SYSTEM_FAILURE_DESTN_CHECK
	};
}
