/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen;

import java.math.BigDecimal;

import com.gauravk.acctxfer.gen.AcctTransferRequest;

public class EuropeAccountingEntries extends AccountingEntriesAbstract {
	
	private static AcctEntryGeneratorInterface<AcctTransferRequest> instance;

	public static AcctEntryGeneratorInterface<AcctTransferRequest> getInstance() {

		if (instance == null) {
			synchronized (EuropeAccountingEntries.class) {
				if (instance == null) {
					instance = new EuropeAccountingEntries();
				}
			}
		}

		return instance;
	}

	protected String formatCreditAccountingEntry(AcctTransferRequest request, BigDecimal amount) {

		String entry = "CREDIT [" + request.getDestinIBanNum()+ "] for [" + amount + "]";

		return entry;
	}

	protected String formatDebitAccountingEntry(AcctTransferRequest request, BigDecimal amount) {

		String entry = "DEBIT [" + request.getSourceIBanNum() + "] for [" + amount + "]";

		return entry;
	}


}
