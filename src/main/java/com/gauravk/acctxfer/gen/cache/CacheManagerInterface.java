/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen.cache;

import java.util.Map;
import java.util.Set;

import com.gauravk.acctxfer.gen.AcctTransferRequest;

public interface CacheManagerInterface {

	String fetch(String key);
	Set<String> getRestrictedCountries();
	
	Map<String, AcctTransferRequest> getAchInfoDetails();
	void setAchInfoDetails(Map<String, AcctTransferRequest> achInfoDetails);

}
