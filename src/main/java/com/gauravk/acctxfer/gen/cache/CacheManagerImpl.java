/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */

package com.gauravk.acctxfer.gen.cache;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.gauravk.acctxfer.gen.AcctTransferRequest;

public class CacheManagerImpl implements CacheManagerInterface {

	private static CacheManagerInterface instance;

	private static Set<String> restrictedCountries;

	public static final String BANK_INTERNAL_ACCT_NUMBER_KEY = "BANK_INTERNAL_ACCT_NUMBER_KEY";
	private static String BANK_INTERNAL_ACCT_NUMBER_VAL;

	public static final String BANK_ROUTING_KEY = "BANK_ROUTING_NUMBER_KEY";
	private static String BANK_ROUTING_NUMBER_VAL;

	private Map<String, String> entries;

	private volatile Map<String, AcctTransferRequest> achInfoDetails = new ConcurrentHashMap<String, AcctTransferRequest>();

	static {

		FileReader fr = null;
		BufferedReader reader = null;

		if (restrictedCountries == null) {

			try {

				synchronized (CacheManagerImpl.class) {

					if (restrictedCountries == null) {

						restrictedCountries = new HashSet<String>();
						try {

							File fi = new File(CacheManagerImpl.class.getClassLoader().getResource("restrictedCountries.txt").getFile());
							fr = new FileReader(fi);
							reader = new BufferedReader(fr);

							while (true) {
								String str = reader.readLine();

								if (str != null && !str.isEmpty()) {

									restrictedCountries.add(str);
								} else {
									break;
								}
							}

						} catch (IOException e) {
							e.printStackTrace();
						} finally {
							try {
								reader.close();
								fr.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}

					}
				}
			} catch (Exception ex) {
				System.out.println("WARNING: Unable to read the file restrictedCountries.txt. Will use hard-coded list");
			} finally {
				if (restrictedCountries == null) {
					synchronized (CacheManagerImpl.class) {
						if (restrictedCountries == null) {
							restrictedCountries = new HashSet<String>();
							restrictedCountries.addAll(Arrays.asList("R_CTRY_A", "R_CTRY_B", "R_CTRY_C", "R_CTRY_D"));
						}
					}
				}
			}
		}

		if (BANK_INTERNAL_ACCT_NUMBER_VAL == null || BANK_ROUTING_NUMBER_VAL == null) {

			try {
				synchronized (CacheManagerImpl.class) {
					if (BANK_INTERNAL_ACCT_NUMBER_VAL == null || BANK_ROUTING_NUMBER_VAL == null) {
						InputStream inputStream = CacheManagerImpl.class.getClassLoader().getResourceAsStream("bankDetails.properties");

						Properties properties = new Properties();

						try {
							properties.load(inputStream);
							BANK_INTERNAL_ACCT_NUMBER_VAL = properties.getProperty("BANK_INTERNAL_ACCT_NUMBER_KEY");
							BANK_ROUTING_NUMBER_VAL = properties.getProperty("BANK_ROUTING_NUMBER_KEY");

						} catch (IOException e) {
							e.printStackTrace();
						} finally {
							try {
								inputStream.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
				}
			} catch (Exception ex) {
				System.out.println("WARNING : Unable to read bankDetails.properties. Will initialize Bank's internal account number/routing number with hard-coded values");
			} finally {

				if (BANK_INTERNAL_ACCT_NUMBER_VAL == null || BANK_ROUTING_NUMBER_VAL == null) {

					synchronized (CacheManagerImpl.class) {
						if (BANK_INTERNAL_ACCT_NUMBER_VAL == null || BANK_ROUTING_NUMBER_VAL == null) {
							
							BANK_INTERNAL_ACCT_NUMBER_VAL = "1123-1123-1234";
							BANK_ROUTING_NUMBER_VAL = "290129";
							
						}

					}

				}
			}
		}
	}

	public static CacheManagerInterface getInstance() {

		if (instance == null) {
			synchronized (CacheManagerImpl.class) {
				if (instance == null) {
					instance = new CacheManagerImpl();
				}
			}
		}

		return instance;
	}

	@Override
	public String fetch(String key) {

		switch (key) {
		case BANK_INTERNAL_ACCT_NUMBER_KEY:
			return BANK_INTERNAL_ACCT_NUMBER_VAL;
		case BANK_ROUTING_KEY:
			return BANK_ROUTING_NUMBER_VAL;
		default:
			return entries.get(key);
		}
	}

	@Override
	public Set<String> getRestrictedCountries() {
		return restrictedCountries;
	}

	public static void setRestrictedCountries(Set<String> restrictedCountries) {
		CacheManagerImpl.restrictedCountries = restrictedCountries;
	}

	public static void main(String[] args) {
		System.out.println("restrictedCountries :" + restrictedCountries);
	}

	@Override
	public Map<String, AcctTransferRequest> getAchInfoDetails() {
		return achInfoDetails;
	}

	@Override
	public void setAchInfoDetails(Map<String, AcctTransferRequest> achInfoDetails) {
		this.achInfoDetails = achInfoDetails;
	}
}
