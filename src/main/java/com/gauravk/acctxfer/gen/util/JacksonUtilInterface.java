/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.gauravk.acctxfer.gen.AcctTransferRequest;
import com.gauravk.acctxfer.gen.AcctTransferNotificationRequest;
import com.gauravk.acctxfer.gen.AcctTransferNotificationResponse;
import com.gauravk.acctxfer.gen.AcctTransferResponse;
import com.gauravk.acctxfer.wire.WireTransferRequest;
import com.gauravk.acctxfer.wire.WireTransferResponse;

public interface JacksonUtilInterface {
	
	WireTransferRequest parseWire(String body) throws JsonParseException, JsonMappingException, IOException;
	
	AcctTransferRequest parseAch(String body) throws JsonParseException, JsonMappingException, IOException;

	String writeWire(WireTransferRequest request) throws JsonParseException, JsonMappingException, IOException;

	String writeWire(WireTransferResponse response) throws JsonParseException, JsonMappingException, IOException;

	String writeAch(AcctTransferRequest request) throws JsonParseException, JsonMappingException, IOException;

	String writeAch(AcctTransferResponse response) throws JsonParseException, JsonMappingException, IOException;

	AcctTransferNotificationRequest parseAchNotification(String body) throws JsonParseException, JsonMappingException, IOException;

	String writeAchNotification(AcctTransferNotificationResponse response) throws JsonParseException, JsonMappingException, IOException;
	String writeAchNotificationReq(AcctTransferNotificationRequest req) throws JsonParseException, JsonMappingException, IOException;
}
