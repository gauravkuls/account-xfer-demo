/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gauravk.acctxfer.gen.AcctTransferRequest;
import com.gauravk.acctxfer.gen.AcctTransferNotificationRequest;
import com.gauravk.acctxfer.gen.AcctTransferNotificationResponse;
import com.gauravk.acctxfer.gen.AcctTransferResponse;
import com.gauravk.acctxfer.wire.WireTransferRequest;
import com.gauravk.acctxfer.wire.WireTransferResponse;

public class JacksonUtil implements JacksonUtilInterface{

	private static JacksonUtilInterface instance;
	
	public static JacksonUtilInterface getInstance() {
		
		if(instance==null) {
			synchronized(JacksonUtil.class) {
				if(instance==null) {
					instance = new JacksonUtil();
				}
			}
		}
		
		return instance;
	}

	@Override
	public WireTransferRequest parseWire(String body) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		return om.readValue(body,WireTransferRequest.class );
	}
	
	@Override
	public AcctTransferRequest parseAch(String body) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		return om.readValue(body,AcctTransferRequest.class );
	}
	
	@Override
	public String writeWire(WireTransferRequest request) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		String retVal = om.writeValueAsString(request);
		
		System.out.println("Return Value :"+retVal);
		
		return retVal;
	}
	
	@Override
	public String writeWire(WireTransferResponse response) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		String retVal = om.writeValueAsString(response);
		
		System.out.println("Return Value :"+retVal);
		
		return retVal;
	}
	
	@Override
	public String writeAch(AcctTransferRequest request) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		String retVal = om.writeValueAsString(request);
		
		System.out.println("Return Value :"+retVal);
		
		return retVal;
	}
	
	@Override
	public String writeAch(AcctTransferResponse response) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		String retVal = om.writeValueAsString(response);
		
		System.out.println("Return Value :"+retVal);
		
		return retVal;
	}

	@Override
	public AcctTransferNotificationRequest parseAchNotification(String body) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		return om.readValue(body,AcctTransferNotificationRequest.class );
	}
	
	@Override
	public String writeAchNotification(AcctTransferNotificationResponse response) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		String retVal = om.writeValueAsString(response);
		
		System.out.println("Return Value :"+retVal);
		
		return retVal;
	}

	@Override
	public String writeAchNotificationReq(AcctTransferNotificationRequest req) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		String retVal = om.writeValueAsString(req);
		
		System.out.println("Return Value :"+retVal);
		
		return retVal;
	}
}
