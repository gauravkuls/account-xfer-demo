/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen;

import java.util.Set;

public class AcctTransferNotificationRequest {

	private Set<AccountTransferNotificationSingle> notifications;

	public Set<AccountTransferNotificationSingle> getNotifications() {
		return notifications;
	}

	public void setNotifications(Set<AccountTransferNotificationSingle> notifications) {
		this.notifications = notifications;
	}

}
