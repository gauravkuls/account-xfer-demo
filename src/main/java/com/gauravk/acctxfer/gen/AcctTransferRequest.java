/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen;

public class AcctTransferRequest {

	private String originName;
	private String originCountry;
	private String orgntrPhoneNumber;
	private String orgntrAddress;
	
	private String destinName;
	private String destinCountry;
	private String destinPhoneNum;
	private String destnAddress;
	
	private String transferAmt;
	
	private String sourceIBanNum;
	private String destinIBanNum;
	
	private String originRoutingNumber;
	private String originAccountNumber;
	
	private String destinRoutingNumber;
	private String destinAccountNumber;
	
	public String getOriginCountry() {
		return originCountry;
	}
	
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
	
	public String getOrgntrPhoneNumber() {
		return orgntrPhoneNumber;
	}
	
	public void setOrgntrPhoneNumber(String orgntrPhoneNumber) {
		this.orgntrPhoneNumber = orgntrPhoneNumber;
	}
	
	public String getOrgntrAddress() {
		return orgntrAddress;
	}
	
	public void setOrgntrAddress(String orgntrAddress) {
		this.orgntrAddress = orgntrAddress;
	}
	
	public String getDestinCountry() {
		return destinCountry;
	}
	
	public void setDestinCountry(String destinCountry) {
		this.destinCountry = destinCountry;
	}
	
	public String getDestinPhoneNum() {
		return destinPhoneNum;
	}
	
	public void setDestinPhoneNum(String destinPhoneNum) {
		this.destinPhoneNum = destinPhoneNum;
	}
	
	public String getDestnAddress() {
		return destnAddress;
	}
	
	public void setDestnAddress(String destnAddress) {
		this.destnAddress = destnAddress;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public String getDestinName() {
		return destinName;
	}

	public void setDestinName(String destinName) {
		this.destinName = destinName;
	}

	public String getTransferAmt() {
		return transferAmt;
	}

	public void setTransferAmt(String transferAmt) {
		this.transferAmt = transferAmt;
	}

	public String getSourceIBanNum() {
		return sourceIBanNum;
	}

	public void setSourceIBanNum(String sourceIBanNum) {
		this.sourceIBanNum = sourceIBanNum;
	}

	public String getDestinIBanNum() {
		return destinIBanNum;
	}

	public void setDestinIBanNum(String destinIBanNum) {
		this.destinIBanNum = destinIBanNum;
	}
	
	public String getOriginRoutingNumber() {
		return originRoutingNumber;
	}

	public void setOriginRoutingNumber(String originRoutingNumber) {
		this.originRoutingNumber = originRoutingNumber;
	}

	public String getOriginAccountNumber() {
		return originAccountNumber;
	}

	public void setOriginAccountNumber(String originAccountNumber) {
		this.originAccountNumber = originAccountNumber;
	}

	public String getDestinRoutingNumber() {
		return destinRoutingNumber;
	}

	public void setDestinRoutingNumber(String destinRoutingNumber) {
		this.destinRoutingNumber = destinRoutingNumber;
	}

	public String getDestinAccountNumber() {
		return destinAccountNumber;
	}

	public void setDestinAccountNumber(String destinAccountNumber) {
		this.destinAccountNumber = destinAccountNumber;
	}
}
