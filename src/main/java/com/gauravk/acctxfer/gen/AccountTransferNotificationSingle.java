/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen;

public class AccountTransferNotificationSingle {
	private String token;

	public enum TRANSFER_STATUS {
		COMPLETE, FAILED
	};

	private TRANSFER_STATUS transferStatus;
	
	private String errorMessage;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public TRANSFER_STATUS getTransferStatus() {
		return transferStatus;
	}

	public void setTransferStatus(TRANSFER_STATUS transferStatus) {
		this.transferStatus = transferStatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountTransferNotificationSingle other = (AccountTransferNotificationSingle) obj;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		return true;
	}
	
	
}
