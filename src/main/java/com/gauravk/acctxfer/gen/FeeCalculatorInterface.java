/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen;

import java.math.BigDecimal;

public interface FeeCalculatorInterface {
	
	public BigDecimal calculateFees(BigDecimal amount);

}
