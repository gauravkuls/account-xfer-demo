/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen;

import java.math.BigDecimal;
import java.util.List;

public interface AcctEntryGeneratorInterface <T extends AcctTransferRequest> {

	List<String> makeAccountingEntriesInternal(T request, BigDecimal fees);
	
	List<String> makeAccountingEntriesOutgoing(T request, BigDecimal fees);
}
