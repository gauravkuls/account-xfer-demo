/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen.vldn;

import java.math.BigDecimal;

import com.gauravk.acctxfer.gen.Account;
import com.gauravk.acctxfer.gen.AcctTransferRequest;
import com.gauravk.acctxfer.gen.TransferManagerInterface.FIN_VALIDN_STATUS;
import com.gauravk.acctxfer.wire.extgateway.AccountInfoImpl;
import com.gauravk.acctxfer.wire.extgateway.AccountInfoInterface;

public class FinancialValidatorImpl implements FinancialValidatorInterface {

	private AccountInfoInterface accountInfoImpl;

	private static FinancialValidatorInterface instance;

	public static FinancialValidatorInterface getInstance() {

		if (instance == null) {
			synchronized (FinancialValidatorImpl.class) {
				if (instance == null) {
					instance = new FinancialValidatorImpl(AccountInfoImpl.getInstance());
				}
			}
		}

		return instance;
	}

	public static FinancialValidatorInterface getInstance(AccountInfoInterface accountInfoImpl) {

		if (instance == null) {
			synchronized (FinancialValidatorImpl.class) {
				if (instance == null) {
					instance = new FinancialValidatorImpl(accountInfoImpl);
				}
			}
		}

		return instance;
	}
	
	private FinancialValidatorImpl(AccountInfoInterface accountInfoImpl) {
		this.accountInfoImpl = accountInfoImpl;
	}

	@Override
	public FIN_VALIDN_STATUS validate(AcctTransferRequest request) {
		Account srcAccount = accountInfoImpl.fetchSourceAccountDetails(request);

		BigDecimal transferAmt = new BigDecimal(request.getTransferAmt());

		if (srcAccount.getAccountStatus() != Account.ACCOUNT_STATUS.ACTIVE) {

			return FIN_VALIDN_STATUS.SRC_ACCT_INVALID;
		}

		if ((srcAccount.getBalance().compareTo(transferAmt) < 0)) {
			return FIN_VALIDN_STATUS.SRC_INSUFF_FUNDS;
		}

		Account destAccount = accountInfoImpl.fetchDestinAccountDetails(request);
		if ((destAccount.getAccountStatus() != Account.ACCOUNT_STATUS.ACTIVE)) {
			return FIN_VALIDN_STATUS.DESTN_ACCT_INVALID;
		}

		return FIN_VALIDN_STATUS.SUCCESS;

	}

}
