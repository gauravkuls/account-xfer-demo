/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen.vldn;

import com.gauravk.acctxfer.gen.AcctTransferRequest;
import com.gauravk.acctxfer.gen.TransferManagerInterface.FIN_VALIDN_STATUS;

public interface FinancialValidatorInterface {
	
	FIN_VALIDN_STATUS validate(AcctTransferRequest request);

}
