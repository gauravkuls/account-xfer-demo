/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen;

import java.math.BigDecimal;

import com.gauravk.acctxfer.gen.AcctTransferRequest;

public class USAccountingEntries extends AccountingEntriesAbstract {
	
	

	public static AcctEntryGeneratorInterface<AcctTransferRequest> getInstance() {

		if (instance == null) {
			synchronized (USAccountingEntries.class) {
				if (instance == null) {
					instance = new USAccountingEntries();
				}
			}
		}

		return instance;
	}	

	protected String formatCreditAccountingEntry(AcctTransferRequest request, BigDecimal amount) {

		String entry = "CREDIT [" + request.getDestinRoutingNumber() + "]-[" + request.getDestinAccountNumber() + "] for [" + amount + "]";

		return entry;
	}

	protected String formatDebitAccountingEntry(AcctTransferRequest request, BigDecimal amount) {

		String entry = "DEBIT [" + request.getOriginRoutingNumber() + "]-[" + request.getOriginAccountNumber() + "] for [" + amount + "]";

		return entry;
	}

}
