/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import com.gauravk.acctxfer.gen.cache.CacheManagerImpl;
import com.gauravk.acctxfer.gen.cache.CacheManagerInterface;

public abstract class AccountingEntriesAbstract implements AcctEntryGeneratorInterface<AcctTransferRequest>{
	
	protected static AcctEntryGeneratorInterface<AcctTransferRequest> instance;
	
	@Override
	public List<String> makeAccountingEntriesInternal(AcctTransferRequest request, BigDecimal fees) {
		
		BigDecimal transferAmt = new BigDecimal(request.getTransferAmt());
		
		String debit1 = formatDebitAccountingEntry(request, fees.add(transferAmt));
		
		String credit1 = formatCreditAccountingEntryInternal(request, fees.add(transferAmt));
		
		List<String> acctEntries = Arrays.asList(debit1,credit1);
		
		return acctEntries;
	}
	
	@Override
	public List<String> makeAccountingEntriesOutgoing(AcctTransferRequest request, BigDecimal fees) {
		
		BigDecimal transferAmt = new BigDecimal(request.getTransferAmt());
		
		String debit2 = formatDebitAccountingEntryInternal(request, transferAmt);
		String credit2 = formatCreditAccountingEntry(request, transferAmt);
		
		List<String> acctEntries = Arrays.asList(debit2,credit2);
		return acctEntries;
		
	}
	
	protected String formatCreditAccountingEntryInternal(AcctTransferRequest request, BigDecimal amount) {

		CacheManagerInterface cacheMgr = CacheManagerImpl.getInstance();

		String entry = "CREDIT [" + cacheMgr.fetch(CacheManagerImpl.BANK_ROUTING_KEY) + "]-[" + cacheMgr.fetch(CacheManagerImpl.BANK_INTERNAL_ACCT_NUMBER_KEY) + "] for [" + amount + "]";

		return entry;
	}
	
	protected String formatDebitAccountingEntryInternal(AcctTransferRequest request, BigDecimal amount) {

		CacheManagerInterface cacheMgr = CacheManagerImpl.getInstance();

		String entry = "DEBIT [" + cacheMgr.fetch(CacheManagerImpl.BANK_ROUTING_KEY) + "]-[" + cacheMgr.fetch(CacheManagerImpl.BANK_INTERNAL_ACCT_NUMBER_KEY) + "] for [" + amount + "]";

		return entry;
	}

	protected abstract String formatCreditAccountingEntry(AcctTransferRequest request, BigDecimal amount);
	
	protected abstract String formatDebitAccountingEntry(AcctTransferRequest request, BigDecimal amount) ;
}
