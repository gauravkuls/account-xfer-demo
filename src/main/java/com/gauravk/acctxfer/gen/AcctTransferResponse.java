/*
 * (C) Copyright 2020 Gaurav K
 * 
 */
		
package com.gauravk.acctxfer.gen;

public class AcctTransferResponse {
	public AcctTransferResponse(String transactionRefCode) {
		super();
		this.transactionRefCode = transactionRefCode;
	}

	private String transactionRefCode;

	public String getTransactionRefCode() {
		return transactionRefCode;
	}

	public void setTransactionRefCode(String transactionRefCode) {
		this.transactionRefCode = transactionRefCode;
	}
	
}
