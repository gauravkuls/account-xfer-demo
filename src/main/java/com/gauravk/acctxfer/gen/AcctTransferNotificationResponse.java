/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen;

public class AcctTransferNotificationResponse {

	public enum ACK_STATUS{SUCCESS, FAILURE};
	
	private ACK_STATUS ackStatus;

	public ACK_STATUS getAckStatus() {
		return ackStatus;
	}

	public void setAckStatus(ACK_STATUS ackStatus) {
		this.ackStatus = ackStatus;
	}
	
}
