/*
 * (C) Copyright 2020 Gaurav K
 * 
 * This software is only a demo and should not be used for any business purpose.
 * 
 */
		
package com.gauravk.acctxfer;

import static spark.Spark.post;
import static spark.Spark.threadPool;
import com.gauravk.acctxfer.ctlr.AchTransferCtlr;
import com.gauravk.acctxfer.ctlr.AchTransferNotificationCtlr;
import com.gauravk.acctxfer.ctlr.WireTransferCtlr;

public class AcctTransfer {

	public static void main(String[] args) {

		int maxThreads = 10000;
		threadPool(maxThreads);

		AchTransferCtlr accountTransfercontroller = new AchTransferCtlr();
		post("/achtransfer", accountTransfercontroller);

		AchTransferNotificationCtlr accountNotificationcontroller = new AchTransferNotificationCtlr();
		post("/achtransfer/notification", accountNotificationcontroller);

		WireTransferCtlr wireTransferController = new WireTransferCtlr();
		post("/wiretransfer", wireTransferController);

		System.out.println("Server Started ...");
	}

}
