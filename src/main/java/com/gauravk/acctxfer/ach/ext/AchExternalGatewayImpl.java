/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.ach.ext;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

public class AchExternalGatewayImpl implements AchExternalGatewayInterface{

	
	private static AchExternalGatewayInterface instance;

	public static AchExternalGatewayInterface getInstance() {

		if (instance == null) {
			synchronized (AchExternalGatewayImpl.class) {
				if (instance == null) {
					instance = new AchExternalGatewayImpl();
				}
			}
		}

		return instance;
	}
	
	@Override
	public Set<String> postToAch(Map<String, CopyOnWriteArrayList<String>> accountingAchEntries) {		
		
		System.out.println("Posted Accounting entries to ACH :"+accountingAchEntries);
		
		return accountingAchEntries.keySet();
	}

}
