/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.ach.ext;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

public interface AchExternalGatewayInterface {
	
	public Set<String> postToAch(Map<String, CopyOnWriteArrayList<String>> accountingAchEntries);

}
