/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.ach.ext;

import java.util.concurrent.CopyOnWriteArrayList;

import com.gauravk.acctxfer.gen.AcctTransferRequest;

public interface AchBatchExecnMgrInterface {

	String submitBatchProcessing(String tokenId, CopyOnWriteArrayList<String> achAcctEntries, AcctTransferRequest request) throws InterruptedException;
	
	

}
