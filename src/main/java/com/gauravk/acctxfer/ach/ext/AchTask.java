/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.ach.ext;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;

import com.gauravk.acctxfer.gen.AcctTransferRequest;
import com.gauravk.acctxfer.gen.notifcn.NotifierInterface;
import com.gauravk.acctxfer.gen.notifcn.PhoneNotifier;

public class AchTask implements Callable<Set<String>> {

	private volatile Map<String, CopyOnWriteArrayList<String>> achEntriesInternal = null;
	private volatile Map<String, CopyOnWriteArrayList<String>> achEntriesCollectiveInternal = null;
	private volatile Map<String, AcctTransferRequest> achInfoDetailsInternal = null;

	public AchTask(final Map<String, CopyOnWriteArrayList<String>> achEntries, final Map<String, CopyOnWriteArrayList<String>> achEntriesCollective, Map<String, AcctTransferRequest> achInfoDetails) {
		this.achEntriesInternal = achEntries;
		this.achEntriesCollectiveInternal = achEntriesCollective;
		this.achInfoDetailsInternal = achInfoDetails;
	}

	@Override
	public Set<String> call() throws Exception {

		Set<String> tokens = AchExternalGatewayImpl.getInstance().postToAch(achEntriesInternal);

		NotifierInterface notifier = PhoneNotifier.getInstance();

		this.achEntriesCollectiveInternal.putAll(achEntriesInternal);

		this.achEntriesInternal.clear();

		for (String token : tokens) {
			
			AcctTransferRequest request = achInfoDetailsInternal.get(token);

			notifier.notify(request.getOrgntrPhoneNumber(), "Dear " + request.getOriginName() + ",  ACH Transaction :" + token + " has been successfully initiated");
			notifier.notify(request.getDestinPhoneNum(), "Dear " + request.getDestinName() + ", An  ACH Transaction :" + token + " has been initiated");
		}

		return tokens;
	}

	public Map<String, CopyOnWriteArrayList<String>> getAchEntriesInternal() {
		return achEntriesInternal;
	}

	public void setAchEntriesInternal(Map<String, CopyOnWriteArrayList<String>> achEntriesInternal) {
		this.achEntriesInternal = achEntriesInternal;
	}

}