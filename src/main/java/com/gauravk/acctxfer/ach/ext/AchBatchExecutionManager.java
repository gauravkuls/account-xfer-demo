/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.ach.ext;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.gauravk.acctxfer.gen.AcctTransferRequest;
import com.gauravk.acctxfer.gen.cache.CacheManagerImpl;

public class AchBatchExecutionManager implements AchBatchExecnMgrInterface{
	
	private final Map<String, CopyOnWriteArrayList<String>> achEntries = new ConcurrentHashMap<String, CopyOnWriteArrayList<String>>();

	private final Map<String, AcctTransferRequest> achInfoDetails = new ConcurrentHashMap<String, AcctTransferRequest>(); 
	
	private final Map<String, CopyOnWriteArrayList<String>> achEntriesCollective = new ConcurrentHashMap<String, CopyOnWriteArrayList<String>>();

	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	
	private volatile Callable<Set<String>> task = null;
	
	
	private static AchBatchExecnMgrInterface instance;

	public static AchBatchExecnMgrInterface getInstance() {

		if (instance == null) {
			synchronized (AchBatchExecutionManager.class) {
				if (instance == null) {
					instance = new AchBatchExecutionManager();
				}
			}
		}

		return instance;
	}
	
	public AchBatchExecutionManager() {
		achEntriesCollective.clear();
		achInfoDetails.clear();
		init();
		
	}

	public void init() {
		achEntries.clear();
		long delay = calculateDelay();
		task = new AchTask(achEntries, achEntriesCollective, achInfoDetails);
		scheduler.schedule(task, delay, TimeUnit.MINUTES);		
	}
	
	@Override
	public String submitBatchProcessing(String tokenId, CopyOnWriteArrayList<String> achAcctEntries, AcctTransferRequest request) throws InterruptedException {

		this.achEntries.put(tokenId, achAcctEntries);
		this.achInfoDetails.put(tokenId, request);
		
		CacheManagerImpl.getInstance().setAchInfoDetails(achInfoDetails);

		return tokenId;
	}

	private static long calculateDelay() {
		LocalDateTime dateTimeCurr = LocalDateTime.now();
		LocalDateTime targetDateTime = LocalDateTime.of(dateTimeCurr.getYear(), dateTimeCurr.getMonth(), dateTimeCurr.getDayOfMonth(), 16, 0, 0);

		if (dateTimeCurr.isAfter(targetDateTime)) {
			targetDateTime = targetDateTime.plusDays(1);
		}

		long delay = Duration.between(dateTimeCurr, targetDateTime).toMinutes();

		return delay;
	}



}
