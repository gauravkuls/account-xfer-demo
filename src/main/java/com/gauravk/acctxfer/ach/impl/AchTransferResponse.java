/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.ach.impl;

import com.gauravk.acctxfer.gen.AcctTransferResponse;
import com.gauravk.acctxfer.gen.TransferManagerInterface.FIN_VALIDN_STATUS;
import static com.gauravk.acctxfer.ach.impl.AchTransferManagerAbstract.RET_STATUS;
public class AchTransferResponse extends AcctTransferResponse {

	private RET_STATUS srcCtryValidation;
	private RET_STATUS destginCtryValidation;
	private FIN_VALIDN_STATUS finValidStatus;
	private RET_STATUS processDone;
	private RET_STATUS notificnDone;
	private String exceptionMessageProcess;
	private String exceptionMessageNotificn;

	public AchTransferResponse(RET_STATUS srcCtryValidation, RET_STATUS destginCtryValidation,
			FIN_VALIDN_STATUS finValidStatus, RET_STATUS processDone, RET_STATUS notificnDone, String tokenId,
			String exceptionMessageProcess, String exceptionMessageNotificn) {
		
		
		super(tokenId);
		
		this.srcCtryValidation=srcCtryValidation;
		this.destginCtryValidation = destginCtryValidation;
		this.finValidStatus = finValidStatus;
		this.processDone = processDone;
		this.notificnDone = notificnDone;
		this.exceptionMessageProcess = exceptionMessageProcess;
		this.exceptionMessageNotificn = exceptionMessageNotificn;

	}

	public RET_STATUS getSrcCtryValidation() {
		return srcCtryValidation;
	}

	public void setSrcCtryValidation(RET_STATUS srcCtryValidation) {
		this.srcCtryValidation = srcCtryValidation;
	}

	public RET_STATUS getDestginCtryValidation() {
		return destginCtryValidation;
	}

	public void setDestginCtryValidation(RET_STATUS destginCtryValidation) {
		this.destginCtryValidation = destginCtryValidation;
	}

	public FIN_VALIDN_STATUS getFinValidStatus() {
		return finValidStatus;
	}

	public void setFinValidStatus(FIN_VALIDN_STATUS finValidStatus) {
		this.finValidStatus = finValidStatus;
	}

	public RET_STATUS getProcessDone() {
		return processDone;
	}

	public void setProcessDone(RET_STATUS processDone) {
		this.processDone = processDone;
	}

	public RET_STATUS getNotificnDone() {
		return notificnDone;
	}

	public void setNotificnDone(RET_STATUS notificnDone) {
		this.notificnDone = notificnDone;
	}

	public String getExceptionMessageProcess() {
		return exceptionMessageProcess;
	}

	public void setExceptionMessageProcess(String exceptionMessageProcess) {
		this.exceptionMessageProcess = exceptionMessageProcess;
	}

	public String getExceptionMessageNotificn() {
		return exceptionMessageNotificn;
	}

	public void setExceptionMessageNotificn(String exceptionMessageNotificn) {
		this.exceptionMessageNotificn = exceptionMessageNotificn;
	}

}
