/*
 * (C) Copyright 2020 Gaurav K
 * 
 */
		
package com.gauravk.acctxfer.ach.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

import com.gauravk.acctxfer.ach.ext.AchBatchExecnMgrInterface;
import com.gauravk.acctxfer.ach.ext.AchBatchExecutionManager;
import com.gauravk.acctxfer.gen.AcctEntryGeneratorInterface;
import com.gauravk.acctxfer.gen.AcctTransferRequest;
import com.gauravk.acctxfer.gen.EuropeAccountingEntries;
import com.gauravk.acctxfer.gen.USAccountingEntries;

public class AchTransferManagerImpl extends AchTransferManagerAbstract {

	private static AchTransferManagerInterface instance;
	private AchBatchExecnMgrInterface achBatchExecutionManager;
	private AcctEntryGeneratorInterface<AcctTransferRequest> usAccountingEntries;
	private AcctEntryGeneratorInterface<AcctTransferRequest> acctEntriesGeneratorEur;
	public static final String USA = "USA";
	public static final String EUROPE = "EUROPE";
	public static final String EGYPT = "EGYPT";
	public static final String NIGERIA = "NIGERIA";

	private AchTransferManagerImpl(AchBatchExecnMgrInterface achExecMgr, AcctEntryGeneratorInterface<AcctTransferRequest> usAcctEntryGeneratorInterface, AcctEntryGeneratorInterface<AcctTransferRequest> acctEntriesGeneratorEurope) {
		this.achBatchExecutionManager = achExecMgr;
		this.usAccountingEntries = usAcctEntryGeneratorInterface;
		this.acctEntriesGeneratorEur = acctEntriesGeneratorEurope;
	}

	public static AchTransferManagerInterface getInstance() {

		if (instance == null) {
			synchronized (AchTransferManagerImpl.class) {
				if (instance == null) {
					instance = new AchTransferManagerImpl(AchBatchExecutionManager.getInstance(), USAccountingEntries.getInstance(), EuropeAccountingEntries.getInstance());
				}
			}
		}

		return instance;
	}
	
	public static AchTransferManagerInterface getInstance(AchBatchExecnMgrInterface achExecMgr, AcctEntryGeneratorInterface<AcctTransferRequest> usAcctEntryGeneratorInterface, AcctEntryGeneratorInterface<AcctTransferRequest> acctEntriesGeneratorEurope) {

		if (instance == null) {
			synchronized (AchTransferManagerImpl.class) {
				if (instance == null) {
					instance = new AchTransferManagerImpl(achExecMgr, usAcctEntryGeneratorInterface, acctEntriesGeneratorEurope);
				}
			}
		}

		return instance;
	}

	@Override
	protected String doProcess(AcctTransferRequest request, BigDecimal fees) throws InterruptedException {
		String tokenId = UUID.randomUUID().toString();

		List<String> sourceAcctEntries = doProcessSource(request, fees);
		List<String> destinAcctEntries = doProcessDestin(request, fees);

		CopyOnWriteArrayList<String> achAcctEntries = new CopyOnWriteArrayList<String>();
		achAcctEntries.addAll(sourceAcctEntries);
		achAcctEntries.addAll(destinAcctEntries);
		
		System.out.println("sourceAcctEntries :"+sourceAcctEntries);
		System.out.println("destinAcctEntries :"+destinAcctEntries);

		achBatchExecutionManager.submitBatchProcessing(tokenId, achAcctEntries, request);

		return tokenId;
	}

	protected List<String> doProcessDestin(AcctTransferRequest request, BigDecimal fees) {

		List<String> acctEntries = new ArrayList<>();

		String destin = request.getDestinCountry();

		switch (destin) {

		case USA:
			List<String> extlExecutionEntries = usAccountingEntries.makeAccountingEntriesOutgoing(request, fees);

			return extlExecutionEntries;

		case EUROPE:

			List<String> extlExecutionEntriesEur = acctEntriesGeneratorEur.makeAccountingEntriesOutgoing(request, fees);

			return extlExecutionEntriesEur;

		default:
			System.out.println("Process Destination : Ach Account Transfer is soon coming to " + destin);
			break;
		}

		return acctEntries;

	}

	protected List<String> doProcessSource(AcctTransferRequest request, BigDecimal fees) {

		List<String> acctEntries = new ArrayList<>();

		String source = request.getOriginCountry();
		switch (source) {
		case USA:
			List<String> intlExecutionEntries = usAccountingEntries.makeAccountingEntriesInternal(request, fees);

			return intlExecutionEntries;

		case EUROPE:
			List<String> intlExecutionEntriesEur = acctEntriesGeneratorEur.makeAccountingEntriesInternal(request, fees);

			return intlExecutionEntriesEur;

		default:
			System.out.println("Process Source : Ach Account Transfer is soon coming to " + source);
			return acctEntries;
		}

	}

}
