/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.ach.impl;

import java.math.BigDecimal;

import com.gauravk.acctxfer.gen.FeeCalculatorInterface;

public class AchFeeCalculatorImpl implements FeeCalculatorInterface {
	private static FeeCalculatorInterface instance;

	public static FeeCalculatorInterface getInstance() {

		if (instance == null) {
			synchronized (AchFeeCalculatorImpl.class) {
				if (instance == null) {
					instance = new AchFeeCalculatorImpl();
				}
			}
		}

		return instance;
	}

	@Override
	public BigDecimal calculateFees(BigDecimal amount) {
		return amount.multiply(new BigDecimal(".004"));
	}

}
