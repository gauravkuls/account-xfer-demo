/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.ach.impl;

import com.gauravk.acctxfer.gen.AcctTransferRequest;
import com.gauravk.acctxfer.gen.AcctTransferResponse;
import com.gauravk.acctxfer.gen.TransferManagerInterface;

public interface AchTransferManagerInterface extends TransferManagerInterface {
	
	public AcctTransferResponse doAchTransfer(AcctTransferRequest request);

}
