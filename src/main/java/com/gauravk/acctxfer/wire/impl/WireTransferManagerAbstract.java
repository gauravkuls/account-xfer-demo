/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.wire.impl;

import java.math.BigDecimal;
import java.util.Set;

import com.gauravk.acctxfer.gen.cache.CacheManagerImpl;
import com.gauravk.acctxfer.gen.notifcn.NotifierInterface;
import com.gauravk.acctxfer.gen.notifcn.PhoneNotifier;
import com.gauravk.acctxfer.gen.vldn.FinancialValidatorImpl;
import com.gauravk.acctxfer.wire.WireTransferRequest;
import com.gauravk.acctxfer.wire.WireTransferResponse;

public abstract class WireTransferManagerAbstract implements WireTransferManagerInterface {

	public enum RET_STATUS {
		SUCCESS, FAILURE
	};

	private static Set<String> restrictedSourceCountries;
	private static Set<String> restrictedDestinCountries;

	static {
		restrictedSourceCountries = CacheManagerImpl.getInstance().getRestrictedCountries();
		restrictedDestinCountries = restrictedSourceCountries;
	}
	
	private RET_STATUS validateSourceCountry(String country) {

		if (restrictedSourceCountries.contains(country)) {
			return RET_STATUS.FAILURE;
		}

		return RET_STATUS.SUCCESS;
	}

	private RET_STATUS validateDestinCountry(String country) {

		if (restrictedDestinCountries.contains(country)) {
			return RET_STATUS.FAILURE;
		}

		return RET_STATUS.SUCCESS;
	}

	@Override
	public WireTransferResponse doWireTransfer(WireTransferRequest request) {
		
		RET_STATUS srcCtryValidation = validateSourceCountry(request.getOriginCountry());
		RET_STATUS destinCtryValidation = validateDestinCountry(request.getDestinCountry());
		
		FIN_VALIDN_STATUS finValidStatus = null;

		RET_STATUS processDone = RET_STATUS.FAILURE;
		RET_STATUS notificnDone = null;

		String exceptionMessageProcess = null;
		String exceptionMessageNotificn = null;
		String tokenId = null;

		if (srcCtryValidation == RET_STATUS.SUCCESS && destinCtryValidation == RET_STATUS.SUCCESS) {
			
			finValidStatus = checkFinancialValidations(request);

			if (finValidStatus == FIN_VALIDN_STATUS.SUCCESS) {
				try {
					tokenId = doProcess(request, WireFeeCalculatorImpl.getInstance().calculateFees(new BigDecimal(request.getTransferAmt())));
					processDone = RET_STATUS.SUCCESS;
				} catch (Exception ex) {
					exceptionMessageProcess = ex.getMessage();
				}
			}
		}
		
		try {
			
			sendNotifications(tokenId, request.getOriginName(), request.getDestinName(), request.getOrgntrPhoneNumber(), request.getDestinPhoneNum(), processDone, exceptionMessageProcess, srcCtryValidation, destinCtryValidation, finValidStatus);
			
			notificnDone = RET_STATUS.SUCCESS;
			
		} catch (Exception ex) {
			exceptionMessageNotificn = ex.getMessage();
		}
		
		return wrapResponse(srcCtryValidation, destinCtryValidation, finValidStatus, processDone, notificnDone, tokenId, exceptionMessageProcess, exceptionMessageNotificn);
	}

	private WireTransferResponse wrapResponse(RET_STATUS srcCtryValidation, RET_STATUS destginCtryValidation, FIN_VALIDN_STATUS finValidStatus, RET_STATUS processDone, RET_STATUS notificnDone, String tokenId, String exceptionMessageProcess, String exceptionMessageNotificn) {

		return new WireTransferResponse(srcCtryValidation, destginCtryValidation, finValidStatus, processDone, notificnDone, tokenId, exceptionMessageProcess, exceptionMessageNotificn);
	}

	protected void sendNotifications(String tokenId, String originName, String destinName, String orgntrPhoneNumber, String destinPhoneNum, RET_STATUS processDone, String exceptionMessageProcess, RET_STATUS srcCtryValidation, RET_STATUS destinCtryValidation, FIN_VALIDN_STATUS finValidStatus) {
		NotifierInterface phoneNotifier = PhoneNotifier.getInstance();

		if (processDone == RET_STATUS.SUCCESS) {
		
			phoneNotifier.notify(orgntrPhoneNumber, "Dear " + originName + " Your transaction has been initiated successfully. Token Id is :" + tokenId);
			phoneNotifier.notify(destinPhoneNum, "Dear " + destinName + ", " + originName + "has initiated transaction for you successfully. Token Id is :" + tokenId);
		
		} else {

			StringBuilder message = new StringBuilder();
			message.append("Dear " + originName + ", Your transaction has failed. Please find the details below:\n");

			if (srcCtryValidation == RET_STATUS.FAILURE) {
				message.append("The country where the transaction has initiated is restricted.\n");
			}

			if (destinCtryValidation == RET_STATUS.FAILURE) {
				message.append("The destination country where the money transfer has been requested is restricted.\n");
			}

			if (srcCtryValidation == RET_STATUS.SUCCESS && destinCtryValidation == RET_STATUS.SUCCESS && finValidStatus != null) {
				if (finValidStatus != FIN_VALIDN_STATUS.SUCCESS) {
					message.append("Financial validation failed :" + finValidStatus);
				}
			}
			
			phoneNotifier.notify(orgntrPhoneNumber, "Dear " + originName + " Your transaction has failed. Error message is :" + message);

		}
	}

	protected FIN_VALIDN_STATUS checkFinancialValidations(WireTransferRequest request) {
		
		return FinancialValidatorImpl.getInstance().validate(request);
	}

	protected abstract String doProcess(WireTransferRequest request, BigDecimal fees);

}
