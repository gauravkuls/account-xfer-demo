/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */

package com.gauravk.acctxfer.wire.impl;

import java.math.BigDecimal;
import java.util.List;

import com.gauravk.acctxfer.gen.AcctEntryGeneratorInterface;
import com.gauravk.acctxfer.gen.AcctTransferRequest;
import com.gauravk.acctxfer.gen.EuropeAccountingEntries;
import com.gauravk.acctxfer.gen.USAccountingEntries;
import com.gauravk.acctxfer.gen.notifcn.EmailNotifier;
import com.gauravk.acctxfer.wire.WireTransferRequest;
import com.gauravk.acctxfer.wire.WireTransferRequest.WIRE_COUNTRY;
import com.gauravk.acctxfer.wire.extgateway.EuropeGateWayInterfaceImpl;
import com.gauravk.acctxfer.wire.extgateway.GateWayInterface;
import com.gauravk.acctxfer.wire.extgateway.USGateWayInterfaceImpl;

public class WireTransferManagerImpl extends WireTransferManagerAbstract {

	private static WireTransferManagerInterface instance;

	private AcctEntryGeneratorInterface<AcctTransferRequest> usAccountingEntries;
	private AcctEntryGeneratorInterface<AcctTransferRequest> acctEntriesGeneratorEur;
	
	private GateWayInterface usGateWayInterfaceImpl;
	private GateWayInterface europeGateWayInterfaceImpl;

	private WireTransferManagerImpl(AcctEntryGeneratorInterface<AcctTransferRequest> usAccountingEntriesIn, AcctEntryGeneratorInterface<AcctTransferRequest> acctEntriesGeneratorEurIn, GateWayInterface usGateWayInterfaceImplIn, GateWayInterface europeGateWayInterfaceImplIn) {
		this.usAccountingEntries = usAccountingEntriesIn;
		this.acctEntriesGeneratorEur = acctEntriesGeneratorEurIn;
		this.usGateWayInterfaceImpl = usGateWayInterfaceImplIn;
		this.europeGateWayInterfaceImpl = europeGateWayInterfaceImplIn;
	}

	public static WireTransferManagerInterface getInstance() {

		if (instance == null) {
			synchronized (EmailNotifier.class) {
				if (instance == null) {
					instance = new WireTransferManagerImpl(USAccountingEntries.getInstance(), EuropeAccountingEntries.getInstance(),  USGateWayInterfaceImpl.getInstance(), EuropeGateWayInterfaceImpl.getInstance());
				}
			}
		}

		return instance;
	}

	public static WireTransferManagerInterface getInstance(AcctEntryGeneratorInterface<AcctTransferRequest> usAccountingEntriesIn, AcctEntryGeneratorInterface<AcctTransferRequest> acctEntriesGeneratorEurIn, GateWayInterface usGateWayInterfaceImplIn, GateWayInterface europeGateWayInterfaceImplIn) {

		if (instance == null) {
			synchronized (EmailNotifier.class) {
				if (instance == null) {
					instance = new WireTransferManagerImpl(usAccountingEntriesIn, acctEntriesGeneratorEurIn,  usGateWayInterfaceImplIn, europeGateWayInterfaceImplIn);
				}
			}
		}

		return instance;
	}
	
	@Override
	protected String doProcess(WireTransferRequest request, BigDecimal fees) {
		WireTransferRequest.WIRE_COUNTRY source = request.getWireSourceCountry();
		WireTransferRequest.WIRE_COUNTRY destin = request.getWireDestinCountry();

		doProcessSource(source, request, fees);
		String tokenId = doProcessDestin(destin, request, fees);

		return tokenId;
	}

	protected String doProcessDestin(WIRE_COUNTRY destin, WireTransferRequest request, BigDecimal fees) {

		String tokenId = null;

		switch (destin) {

		case USA:
			List<String> extlExecutionEntries = usAccountingEntries.makeAccountingEntriesOutgoing(request, fees);
			tokenId = usGateWayInterfaceImpl.postExternalEntries(request, extlExecutionEntries);

			break;

		case EUROPE:
			List<String> extlExecutionEntriesEur = acctEntriesGeneratorEur.makeAccountingEntriesOutgoing(request, fees);
			tokenId = europeGateWayInterfaceImpl.postExternalEntries(request, extlExecutionEntriesEur);
			break;

		case OTHERS:
			System.out.println("Process Destination : Account Transfer is soon coming to " + destin);
			break;
		}

		return tokenId;

	}

	protected void doProcessSource(WIRE_COUNTRY source, WireTransferRequest request, BigDecimal fees) {
		switch (source) {
		case USA:
			List<String> intlExecutionEntries = usAccountingEntries.makeAccountingEntriesInternal(request, fees);
			usGateWayInterfaceImpl.postAccountingEntriesInternal(request, intlExecutionEntries);

			break;

		case EUROPE:
			List<String> intlExecutionEntriesEur = acctEntriesGeneratorEur.makeAccountingEntriesInternal(request, fees);
			europeGateWayInterfaceImpl.postAccountingEntriesInternal(request, intlExecutionEntriesEur);

			break;

		case OTHERS:
			System.out.println("Process Source : Account Transfer is soon coming to " + source);
			break;
		}

	}

}
