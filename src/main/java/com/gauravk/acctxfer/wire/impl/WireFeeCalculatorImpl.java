/*
 * (C) Copyright 2020 Gaurav K
 * 
 */
		
package com.gauravk.acctxfer.wire.impl;

import java.math.BigDecimal;

import com.gauravk.acctxfer.gen.FeeCalculatorInterface;

public class WireFeeCalculatorImpl implements FeeCalculatorInterface {
	private static FeeCalculatorInterface instance;

	public static FeeCalculatorInterface getInstance() {

		if (instance == null) {
			synchronized (WireFeeCalculatorImpl.class) {
				if (instance == null) {
					instance = new WireFeeCalculatorImpl();
				}
			}
		}

		return instance;
	}

	@Override
	public BigDecimal calculateFees(BigDecimal amount) {
		return amount.divide(BigDecimal.TEN);
	}

}
