/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.wire.impl;

import com.gauravk.acctxfer.gen.TransferManagerInterface;
import com.gauravk.acctxfer.wire.WireTransferRequest;
import com.gauravk.acctxfer.wire.WireTransferResponse;

public interface WireTransferManagerInterface extends TransferManagerInterface{
	
	public WireTransferResponse doWireTransfer(WireTransferRequest request);

}
