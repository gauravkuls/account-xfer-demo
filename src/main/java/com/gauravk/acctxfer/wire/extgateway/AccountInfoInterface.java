/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.wire.extgateway;

import com.gauravk.acctxfer.gen.Account;
import com.gauravk.acctxfer.gen.AcctTransferRequest;

public interface AccountInfoInterface {

	Account fetchSourceAccountDetails(AcctTransferRequest request);
	Account fetchDestinAccountDetails(AcctTransferRequest request);
}
