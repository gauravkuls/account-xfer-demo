/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.wire.extgateway;

import java.util.List;

import com.gauravk.acctxfer.wire.WireTransferRequest;

public class EuropeGateWayInterfaceImpl implements GateWayInterface {

	private static EuropeGateWayInterfaceImpl instance;

	public static EuropeGateWayInterfaceImpl getInstance() {

		if (instance == null) {
			synchronized (EuropeGateWayInterfaceImpl.class) {
				if (instance == null) {
					instance = new EuropeGateWayInterfaceImpl();
				}
			}
		}

		return instance;
	}

	@Override
	public String postExternalEntries(WireTransferRequest wireRequest, List<String> extlExecutionEntries) {
		
		return USWireExternalCallerImpl.getInstance().post(wireRequest, extlExecutionEntries);
	}


}
