/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.wire.extgateway;

import java.util.List;

import com.gauravk.acctxfer.wire.WireTransferRequest;

public interface WireExternalCallerInterface {

	public String post(WireTransferRequest request, List<String> acctEntries);
	
}
