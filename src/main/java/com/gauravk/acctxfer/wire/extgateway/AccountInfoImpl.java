/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.wire.extgateway;

import java.math.BigDecimal;

import com.gauravk.acctxfer.gen.Account;
import com.gauravk.acctxfer.gen.AcctTransferRequest;

public class AccountInfoImpl implements AccountInfoInterface {
	private static AccountInfoInterface instance;

	public static AccountInfoInterface getInstance() {

		if (instance == null) {
			synchronized (AccountInfoImpl.class) {
				if (instance == null) {
					instance = new AccountInfoImpl();
				}
			}
		}

		return instance;
	}

	@Override
	public Account fetchSourceAccountDetails(AcctTransferRequest request) {
		Account account = new Account();
		account.setAccountStatus(Account.ACCOUNT_STATUS.ACTIVE);
		account.setBalance(new BigDecimal("9080000"));
		return account;
	}

	@Override
	public Account fetchDestinAccountDetails(AcctTransferRequest request) {
		Account account = new Account();
		account.setAccountStatus(Account.ACCOUNT_STATUS.ACTIVE);
		account.setBalance(new BigDecimal("1000"));
		return account;
	}

}
