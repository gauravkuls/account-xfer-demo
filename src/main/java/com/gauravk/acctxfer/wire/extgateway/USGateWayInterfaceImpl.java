/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.wire.extgateway;

import java.util.List;

import com.gauravk.acctxfer.wire.WireTransferRequest;

public class USGateWayInterfaceImpl implements GateWayInterface {

	private static USGateWayInterfaceImpl instance;

	public static USGateWayInterfaceImpl getInstance() {

		if (instance == null) {
			synchronized (USGateWayInterfaceImpl.class) {
				if (instance == null) {
					instance = new USGateWayInterfaceImpl();
				}
			}
		}

		return instance;
	}

	@Override
	public String postExternalEntries(WireTransferRequest wireRequest, List<String> extlExecutionEntries) {
		
		return USWireExternalCallerImpl.getInstance().post(wireRequest, extlExecutionEntries);
	}


}
