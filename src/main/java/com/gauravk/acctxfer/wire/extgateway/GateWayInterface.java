/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.wire.extgateway;

import java.util.List;

import com.gauravk.acctxfer.wire.WireTransferRequest;

public interface GateWayInterface {

	default void postAccountingEntriesInternal(WireTransferRequest wireRequest, List<String> intlExecutionEntries) {
		System.out.println("Posted Internal Wire Internal Acctng Entries :" + intlExecutionEntries);
	}

	String postExternalEntries(WireTransferRequest wireRequest, List<String> extlExecutionEntries);

}
