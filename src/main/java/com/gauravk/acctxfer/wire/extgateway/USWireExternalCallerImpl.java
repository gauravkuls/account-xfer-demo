/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.wire.extgateway;

import java.util.List;
import java.util.UUID;

import com.gauravk.acctxfer.wire.WireTransferRequest;

public class USWireExternalCallerImpl implements WireExternalCallerInterface {

	private static WireExternalCallerInterface instance;

	public static WireExternalCallerInterface getInstance() {

		if (instance == null) {
			synchronized (USWireExternalCallerImpl.class) {
				if (instance == null) {
					instance = new USWireExternalCallerImpl();
				}
			}
		}

		return instance;
	}

	@Override
	public String post(WireTransferRequest request, List<String> acctEntries) {

		System.out.println("Posted US accounting entries for " + request.getDestinName() + ":" + acctEntries);

		return UUID.randomUUID().toString();
	}

}
