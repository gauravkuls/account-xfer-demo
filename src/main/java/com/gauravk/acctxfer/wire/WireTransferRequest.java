/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.wire;

import com.gauravk.acctxfer.gen.AcctTransferRequest;

public class WireTransferRequest extends AcctTransferRequest{
	
	public enum WIRE_COUNTRY{USA, EUROPE, OTHERS}
	private WIRE_COUNTRY wireSourceCountry;
	private WIRE_COUNTRY wireDestinCountry;
	
	
	public WIRE_COUNTRY getWireSourceCountry() {
		return wireSourceCountry;
	}
	
	public void setWireSourceCountry(WIRE_COUNTRY wireSourceCountry) {
		this.wireSourceCountry = wireSourceCountry;
	}
	
	public WIRE_COUNTRY getWireDestinCountry() {
		return wireDestinCountry;
	}
	
	public void setWireDestinCountry(WIRE_COUNTRY wireDestinCountry) {
		this.wireDestinCountry = wireDestinCountry;
	}



}
