/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.ach.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import org.junit.Test;

import com.gauravk.acctxfer.ach.ext.AchBatchExecnMgrInterface;
import com.gauravk.acctxfer.gen.AcctEntryGeneratorInterface;
import com.gauravk.acctxfer.gen.AcctTransferRequest;
import com.gauravk.acctxfer.gen.EuropeAccountingEntries;
import com.gauravk.acctxfer.gen.USAccountingEntries;

// Run the test cases separately and not together

public class AchTransferManagerImplTest {
	
	@SuppressWarnings("unchecked")
	@Test
	public void testDoProcessUSA() throws Exception {
		AchBatchExecnMgrInterface achExecMgr = mock(AchBatchExecnMgrInterface.class);
		AcctEntryGeneratorInterface<AcctTransferRequest> usAcctEntryGeneratorInterface = mock(USAccountingEntries.class);
		AcctEntryGeneratorInterface<AcctTransferRequest> eurAcctEntryGeneratorInterface = mock(EuropeAccountingEntries.class);
		
		AchTransferManagerInterface achTransferManagerImpl = AchTransferManagerImpl.getInstance(achExecMgr,usAcctEntryGeneratorInterface,eurAcctEntryGeneratorInterface );
		
		
		when(usAcctEntryGeneratorInterface.makeAccountingEntriesInternal(any(AcctTransferRequest.class), any(BigDecimal.class))).thenReturn(new ArrayList<>());
		when(usAcctEntryGeneratorInterface.makeAccountingEntriesOutgoing(any(AcctTransferRequest.class), any(BigDecimal.class))).thenReturn(new ArrayList<>());
		when(achExecMgr.submitBatchProcessing(any(String.class), any(CopyOnWriteArrayList.class), any(AcctTransferRequest.class))).thenReturn("TOKEN");
		
		AcctTransferRequest request = new AcctTransferRequest();
		request.setOriginCountry("USA");
		request.setDestinCountry("USA");
		request.setTransferAmt("1023");
		
		achTransferManagerImpl.doAchTransfer(request);
		
		verify(usAcctEntryGeneratorInterface, times(1)).makeAccountingEntriesInternal(any(AcctTransferRequest.class), any(BigDecimal.class) );
		verify(usAcctEntryGeneratorInterface, times(1)).makeAccountingEntriesOutgoing(any(AcctTransferRequest.class), any(BigDecimal.class) );
		verify(achExecMgr, times(1)).submitBatchProcessing(any(String.class), any(CopyOnWriteArrayList.class), any(AcctTransferRequest.class));
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testDoProcessUSAToEurope() throws Exception {
		AchBatchExecnMgrInterface achExecMgr = mock(AchBatchExecnMgrInterface.class);
		AcctEntryGeneratorInterface<AcctTransferRequest> usAcctEntryGeneratorInterface = mock(USAccountingEntries.class);
		AcctEntryGeneratorInterface<AcctTransferRequest> eurAcctEntryGeneratorInterface = mock(EuropeAccountingEntries.class);
		
		AchTransferManagerInterface achTransferManagerImpl = AchTransferManagerImpl.getInstance(achExecMgr,usAcctEntryGeneratorInterface,eurAcctEntryGeneratorInterface );
		
		
		when(usAcctEntryGeneratorInterface.makeAccountingEntriesInternal(any(AcctTransferRequest.class), any(BigDecimal.class))).thenReturn(new ArrayList<>());
		when(eurAcctEntryGeneratorInterface.makeAccountingEntriesOutgoing(any(AcctTransferRequest.class), any(BigDecimal.class))).thenReturn(new ArrayList<>());
		when(achExecMgr.submitBatchProcessing(any(String.class), any(CopyOnWriteArrayList.class), any(AcctTransferRequest.class))).thenReturn("TOKEN");
		
		AcctTransferRequest request = new AcctTransferRequest();
		request.setOriginCountry("USA");
		request.setDestinCountry("EUROPE");
		request.setTransferAmt("1023");
		
		achTransferManagerImpl.doAchTransfer(request);
		
		verify(usAcctEntryGeneratorInterface, times(1)).makeAccountingEntriesInternal(any(AcctTransferRequest.class), any(BigDecimal.class) );
		verify(eurAcctEntryGeneratorInterface, times(1)).makeAccountingEntriesOutgoing(any(AcctTransferRequest.class), any(BigDecimal.class) );
		verify(achExecMgr, times(1)).submitBatchProcessing(any(String.class), any(CopyOnWriteArrayList.class), any(AcctTransferRequest.class));
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testDoProcessEUR() throws Exception {
		AchBatchExecnMgrInterface achExecMgr = mock(AchBatchExecnMgrInterface.class);
		AcctEntryGeneratorInterface<AcctTransferRequest> usAcctEntryGeneratorInterface = mock(USAccountingEntries.class);
		AcctEntryGeneratorInterface<AcctTransferRequest> eurAcctEntryGeneratorInterface = mock(EuropeAccountingEntries.class);
		
		AchTransferManagerInterface achTransferManagerImpl = AchTransferManagerImpl.getInstance(achExecMgr,usAcctEntryGeneratorInterface,eurAcctEntryGeneratorInterface );
		
		
		when(eurAcctEntryGeneratorInterface.makeAccountingEntriesInternal(any(AcctTransferRequest.class), any(BigDecimal.class))).thenReturn(new ArrayList<>());
		when(eurAcctEntryGeneratorInterface.makeAccountingEntriesOutgoing(any(AcctTransferRequest.class), any(BigDecimal.class))).thenReturn(new ArrayList<>());
		when(achExecMgr.submitBatchProcessing(any(String.class), any(CopyOnWriteArrayList.class), any(AcctTransferRequest.class))).thenReturn("TOKEN");
		
		AcctTransferRequest request = new AcctTransferRequest();
		request.setOriginCountry("EUROPE");
		request.setDestinCountry("EUROPE");
		request.setTransferAmt("1023");
		
		achTransferManagerImpl.doAchTransfer(request);
		
		verify(eurAcctEntryGeneratorInterface, times(1)).makeAccountingEntriesInternal(any(AcctTransferRequest.class), any(BigDecimal.class) );
		verify(eurAcctEntryGeneratorInterface, times(1)).makeAccountingEntriesOutgoing(any(AcctTransferRequest.class), any(BigDecimal.class) );
		verify(achExecMgr, times(1)).submitBatchProcessing(any(String.class), any(CopyOnWriteArrayList.class), any(AcctTransferRequest.class));
	}	
	
	@SuppressWarnings("unchecked")
	@Test
	public void testDoProcessEURToUSA() throws Exception {
		AchBatchExecnMgrInterface achExecMgr = mock(AchBatchExecnMgrInterface.class);
		AcctEntryGeneratorInterface<AcctTransferRequest> usAcctEntryGeneratorInterface = mock(USAccountingEntries.class);
		AcctEntryGeneratorInterface<AcctTransferRequest> eurAcctEntryGeneratorInterface = mock(EuropeAccountingEntries.class);
		
		AchTransferManagerInterface achTransferManagerImpl = AchTransferManagerImpl.getInstance(achExecMgr,usAcctEntryGeneratorInterface,eurAcctEntryGeneratorInterface );
		
		
		when(eurAcctEntryGeneratorInterface.makeAccountingEntriesInternal(any(AcctTransferRequest.class), any(BigDecimal.class))).thenReturn(new ArrayList<>());
		when(usAcctEntryGeneratorInterface.makeAccountingEntriesOutgoing(any(AcctTransferRequest.class), any(BigDecimal.class))).thenReturn(new ArrayList<>());
		when(achExecMgr.submitBatchProcessing(any(String.class), any(CopyOnWriteArrayList.class), any(AcctTransferRequest.class))).thenReturn("TOKEN");
		
		AcctTransferRequest request = new AcctTransferRequest();
		request.setOriginCountry("EUROPE");
		request.setDestinCountry("USA");
		request.setTransferAmt("1023");
		
		achTransferManagerImpl.doAchTransfer(request);
		
		verify(eurAcctEntryGeneratorInterface, times(1)).makeAccountingEntriesInternal(any(AcctTransferRequest.class), any(BigDecimal.class) );
		verify(usAcctEntryGeneratorInterface, times(1)).makeAccountingEntriesOutgoing(any(AcctTransferRequest.class), any(BigDecimal.class) );
		verify(achExecMgr, times(1)).submitBatchProcessing(any(String.class), any(CopyOnWriteArrayList.class), any(AcctTransferRequest.class));
	}

}
