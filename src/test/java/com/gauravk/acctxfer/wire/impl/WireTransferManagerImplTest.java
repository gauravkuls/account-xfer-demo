/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
package com.gauravk.acctxfer.wire.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.gauravk.acctxfer.gen.AcctEntryGeneratorInterface;
import com.gauravk.acctxfer.gen.AcctTransferRequest;
import com.gauravk.acctxfer.gen.EuropeAccountingEntries;
import com.gauravk.acctxfer.gen.USAccountingEntries;
import com.gauravk.acctxfer.wire.WireTransferRequest;
import com.gauravk.acctxfer.wire.extgateway.GateWayInterface;

//Please run all the tests one by one and not together

public class WireTransferManagerImplTest {
	
	@Test
	public void testDoProcessUSAToUSA() throws Exception {
		GateWayInterface usGateWayInterfaceImpl = mock(GateWayInterface.class);
		GateWayInterface europeGateWayInterfaceImpl = mock(GateWayInterface.class);
		
		AcctEntryGeneratorInterface<AcctTransferRequest> usAcctEntryGeneratorInterface = mock(USAccountingEntries.class);
		AcctEntryGeneratorInterface<AcctTransferRequest> eurAcctEntryGeneratorInterface = mock(EuropeAccountingEntries.class);
		
		WireTransferManagerInterface wireTransferManagerImpl = WireTransferManagerImpl.getInstance(usAcctEntryGeneratorInterface, eurAcctEntryGeneratorInterface,  usGateWayInterfaceImpl, europeGateWayInterfaceImpl);
		
		
		when(usAcctEntryGeneratorInterface.makeAccountingEntriesInternal(any(WireTransferRequest.class), any(BigDecimal.class))).thenReturn(new ArrayList<>());
		when(usAcctEntryGeneratorInterface.makeAccountingEntriesOutgoing(any(WireTransferRequest.class), any(BigDecimal.class))).thenReturn(new ArrayList<>());

		doNothing().when(usGateWayInterfaceImpl).postAccountingEntriesInternal(any(WireTransferRequest.class), any(List.class));
		when(usGateWayInterfaceImpl.postExternalEntries(any(WireTransferRequest.class), any(List.class))).thenReturn("TOKEN1");
		
		WireTransferRequest request = new WireTransferRequest();
		request.setOriginCountry("USA");
		request.setWireSourceCountry(WireTransferRequest.WIRE_COUNTRY.USA);
		request.setDestinCountry("USA");
		request.setWireDestinCountry(WireTransferRequest.WIRE_COUNTRY.USA);
		request.setTransferAmt("1023");
		
		request.setOriginName("MS Sender");
		request.setDestinName("MS Receiver");
		
		wireTransferManagerImpl.doWireTransfer(request);
		
		verify(usGateWayInterfaceImpl, times(1)).postAccountingEntriesInternal(any(WireTransferRequest.class), any(List.class) );
		verify(usGateWayInterfaceImpl, times(1)).postExternalEntries(any(WireTransferRequest.class), any(List.class) );
		
		verify(usAcctEntryGeneratorInterface, times(1)).makeAccountingEntriesInternal(any(AcctTransferRequest.class), any(BigDecimal.class) );
		verify(usAcctEntryGeneratorInterface, times(1)).makeAccountingEntriesOutgoing(any(AcctTransferRequest.class), any(BigDecimal.class) );
	}
	
	@Test
	public void testDoProcessEurToEur() throws Exception {
		GateWayInterface usGateWayInterfaceImpl = mock(GateWayInterface.class);
		GateWayInterface europeGateWayInterfaceImpl = mock(GateWayInterface.class);
		
		AcctEntryGeneratorInterface<AcctTransferRequest> usAcctEntryGeneratorInterface = mock(USAccountingEntries.class);
		AcctEntryGeneratorInterface<AcctTransferRequest> eurAcctEntryGeneratorInterface = mock(EuropeAccountingEntries.class);
		
		WireTransferManagerInterface wireTransferManagerImpl = WireTransferManagerImpl.getInstance(usAcctEntryGeneratorInterface, eurAcctEntryGeneratorInterface,  usGateWayInterfaceImpl, europeGateWayInterfaceImpl);
		
		
		when(eurAcctEntryGeneratorInterface.makeAccountingEntriesInternal(any(WireTransferRequest.class), any(BigDecimal.class))).thenReturn(new ArrayList<>());
		when(eurAcctEntryGeneratorInterface.makeAccountingEntriesOutgoing(any(WireTransferRequest.class), any(BigDecimal.class))).thenReturn(new ArrayList<>());

		doNothing().when(europeGateWayInterfaceImpl).postAccountingEntriesInternal(any(WireTransferRequest.class), any(List.class));
		when(europeGateWayInterfaceImpl.postExternalEntries(any(WireTransferRequest.class), any(List.class))).thenReturn("TOKEN1");
		
		WireTransferRequest request = new WireTransferRequest();
		request.setOriginCountry("Europe");
		request.setWireSourceCountry(WireTransferRequest.WIRE_COUNTRY.EUROPE);
		request.setDestinCountry("Europe");
		request.setWireDestinCountry(WireTransferRequest.WIRE_COUNTRY.EUROPE);
		request.setTransferAmt("1023");
		
		request.setOriginName("MS Sender");
		request.setDestinName("MS Receiver");
		
		wireTransferManagerImpl.doWireTransfer(request);
		
		verify(europeGateWayInterfaceImpl, times(1)).postAccountingEntriesInternal(any(WireTransferRequest.class), any(List.class) );
		verify(europeGateWayInterfaceImpl, times(1)).postExternalEntries(any(WireTransferRequest.class), any(List.class) );
		
		verify(eurAcctEntryGeneratorInterface, times(1)).makeAccountingEntriesInternal(any(AcctTransferRequest.class), any(BigDecimal.class) );
		verify(eurAcctEntryGeneratorInterface, times(1)).makeAccountingEntriesOutgoing(any(AcctTransferRequest.class), any(BigDecimal.class) );
	}
	
	@Test
	public void testDoProcessUSAToEur() throws Exception {
		GateWayInterface usGateWayInterfaceImpl = mock(GateWayInterface.class);
		GateWayInterface europeGateWayInterfaceImpl = mock(GateWayInterface.class);
		
		AcctEntryGeneratorInterface<AcctTransferRequest> usAcctEntryGeneratorInterface = mock(USAccountingEntries.class);
		AcctEntryGeneratorInterface<AcctTransferRequest> eurAcctEntryGeneratorInterface = mock(EuropeAccountingEntries.class);
		
		WireTransferManagerInterface wireTransferManagerImpl = WireTransferManagerImpl.getInstance(usAcctEntryGeneratorInterface, eurAcctEntryGeneratorInterface,  usGateWayInterfaceImpl, europeGateWayInterfaceImpl);
		
		
		when(usAcctEntryGeneratorInterface.makeAccountingEntriesInternal(any(WireTransferRequest.class), any(BigDecimal.class))).thenReturn(new ArrayList<>());
		when(eurAcctEntryGeneratorInterface.makeAccountingEntriesOutgoing(any(WireTransferRequest.class), any(BigDecimal.class))).thenReturn(new ArrayList<>());

		doNothing().when(usGateWayInterfaceImpl).postAccountingEntriesInternal(any(WireTransferRequest.class), any(List.class));
		when(europeGateWayInterfaceImpl.postExternalEntries(any(WireTransferRequest.class), any(List.class))).thenReturn("TOKEN1");
		
		WireTransferRequest request = new WireTransferRequest();
		request.setOriginCountry("USA");
		request.setWireSourceCountry(WireTransferRequest.WIRE_COUNTRY.USA);
		request.setDestinCountry("Europe");
		request.setWireDestinCountry(WireTransferRequest.WIRE_COUNTRY.EUROPE);
		request.setTransferAmt("1023");
		
		request.setOriginName("MS Sender");
		request.setDestinName("MS Receiver");
		
		wireTransferManagerImpl.doWireTransfer(request);
		
		verify(usGateWayInterfaceImpl, times(1)).postAccountingEntriesInternal(any(WireTransferRequest.class), any(List.class) );
		verify(europeGateWayInterfaceImpl, times(1)).postExternalEntries(any(WireTransferRequest.class), any(List.class) );
		
		verify(usAcctEntryGeneratorInterface, times(1)).makeAccountingEntriesInternal(any(WireTransferRequest.class), any(BigDecimal.class) );
		verify(eurAcctEntryGeneratorInterface, times(1)).makeAccountingEntriesOutgoing(any(WireTransferRequest.class), any(BigDecimal.class) );
	}
	
	@Test
	public void testDoProcessEurToUSA() throws Exception {
		GateWayInterface usGateWayInterfaceImpl = mock(GateWayInterface.class);
		GateWayInterface europeGateWayInterfaceImpl = mock(GateWayInterface.class);
		
		AcctEntryGeneratorInterface<AcctTransferRequest> usAcctEntryGeneratorInterface = mock(USAccountingEntries.class);
		AcctEntryGeneratorInterface<AcctTransferRequest> eurAcctEntryGeneratorInterface = mock(EuropeAccountingEntries.class);
		
		WireTransferManagerInterface wireTransferManagerImpl = WireTransferManagerImpl.getInstance(usAcctEntryGeneratorInterface, eurAcctEntryGeneratorInterface,  usGateWayInterfaceImpl, europeGateWayInterfaceImpl);
		
		
		when(eurAcctEntryGeneratorInterface.makeAccountingEntriesInternal(any(WireTransferRequest.class), any(BigDecimal.class))).thenReturn(new ArrayList<>());
		when(usAcctEntryGeneratorInterface.makeAccountingEntriesOutgoing(any(WireTransferRequest.class), any(BigDecimal.class))).thenReturn(new ArrayList<>());

		doNothing().when(europeGateWayInterfaceImpl).postAccountingEntriesInternal(any(WireTransferRequest.class), any(List.class));
		when(usGateWayInterfaceImpl.postExternalEntries(any(WireTransferRequest.class), any(List.class))).thenReturn("TOKEN1");
		
		WireTransferRequest request = new WireTransferRequest();
		request.setOriginCountry("Europe");
		request.setWireSourceCountry(WireTransferRequest.WIRE_COUNTRY.EUROPE);
		request.setDestinCountry("USA");
		request.setWireDestinCountry(WireTransferRequest.WIRE_COUNTRY.USA);
		request.setTransferAmt("1023");
		
		request.setOriginName("MS Sender");
		request.setDestinName("MS Receiver");
		
		wireTransferManagerImpl.doWireTransfer(request);
		
		verify(europeGateWayInterfaceImpl, times(1)).postAccountingEntriesInternal(any(WireTransferRequest.class), any(List.class) );
		verify(usGateWayInterfaceImpl, times(1)).postExternalEntries(any(WireTransferRequest.class), any(List.class) );
		
		verify(eurAcctEntryGeneratorInterface, times(1)).makeAccountingEntriesInternal(any(AcctTransferRequest.class), any(BigDecimal.class) );
		verify(usAcctEntryGeneratorInterface, times(1)).makeAccountingEntriesOutgoing(any(AcctTransferRequest.class), any(BigDecimal.class) );
	}	
}
