/*
 * (C) Copyright 2020 Gaurav K
 * 
 */
		
package com.gauravk.acctxfer.gen;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

public class USAccountingEntriesTest {

	@Test
	public void testFormatInternalAccountingEntry() {
		AcctEntryGeneratorInterface<AcctTransferRequest> accountEntriesMgr = USAccountingEntries.getInstance();
		
		BigDecimal fees = new BigDecimal("2.54");
		
		AcctTransferRequest request = new AcctTransferRequest();
		request.setDestinAccountNumber("#destinAccNum");
		request.setDestinRoutingNumber("#destinRoutNum");
		request.setOriginAccountNumber("#originAcctNum");;
		request.setOriginRoutingNumber("#origRoutNum");

		request.setTransferAmt("160");
		
		List<String> acctEntries = accountEntriesMgr.makeAccountingEntriesInternal(request , fees );
		
		assertEquals(2, acctEntries.size());
		assertTrue(acctEntries.contains("DEBIT [#origRoutNum]-[#originAcctNum] for [162.54]"));
		assertTrue(acctEntries.contains("CREDIT [290129]-[1123-1123-1234] for [162.54]"));
		
		System.out.println(acctEntries);
	}
	
	@Test
	public void testFormatOutgoingAccountingEntry() {
		AcctEntryGeneratorInterface<AcctTransferRequest> accountEntriesMgr = USAccountingEntries.getInstance();
		
		BigDecimal fees = new BigDecimal("2.54");
		
		AcctTransferRequest request = new AcctTransferRequest();
		request.setDestinAccountNumber("#destinAccNum");
		request.setDestinRoutingNumber("#destinRoutNum");
		request.setOriginAccountNumber("#originAcctNum");;
		request.setOriginRoutingNumber("#origRoutNum");

		request.setTransferAmt("160");
		
		List<String> acctEntries = accountEntriesMgr.makeAccountingEntriesOutgoing(request, fees);
		
		assertEquals(2, acctEntries.size());
		assertTrue(acctEntries.contains("DEBIT [290129]-[1123-1123-1234] for [160]"));
		assertTrue(acctEntries.contains("CREDIT [#destinRoutNum]-[#destinAccNum] for [160]"));
		
		System.out.println(acctEntries);
	}	
}
