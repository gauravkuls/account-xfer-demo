/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen.vldn;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.Test;

import com.gauravk.acctxfer.gen.Account;
import com.gauravk.acctxfer.gen.AcctTransferRequest;
import com.gauravk.acctxfer.gen.TransferManagerInterface;
import com.gauravk.acctxfer.wire.extgateway.AccountInfoInterface;

//Please run each test case separately

public class FinancialValidatorImplTest {

	@Test
	public void testFinancialValidatorSourceAccountInvalid() {
		
		AccountInfoInterface acctInfo = mock(AccountInfoInterface.class);
		FinancialValidatorInterface testClass1 = FinancialValidatorImpl.getInstance(acctInfo);
		
		Account account = new Account();
		account.setAccountStatus(Account.ACCOUNT_STATUS.INACTIVE);
		
		when(acctInfo.fetchSourceAccountDetails(any(AcctTransferRequest.class))).thenReturn(account);
		
		AcctTransferRequest request = new AcctTransferRequest();
		request.setTransferAmt("1000");
		TransferManagerInterface.FIN_VALIDN_STATUS returnStatus = testClass1.validate(request);
		
		assertEquals(TransferManagerInterface.FIN_VALIDN_STATUS.SRC_ACCT_INVALID, returnStatus);
	}

	@Test
	public void testFinancialValidatorSourceAccountValid() {
		
		AccountInfoInterface acctInfo = mock(AccountInfoInterface.class);
		FinancialValidatorInterface testClass = FinancialValidatorImpl.getInstance(acctInfo);
		
		Account srcAccount = new Account();
		srcAccount.setAccountStatus(Account.ACCOUNT_STATUS.ACTIVE);
		srcAccount.setBalance(new BigDecimal("1000000"));
	
		Account destAccount = new Account();
		destAccount.setAccountStatus(Account.ACCOUNT_STATUS.ACTIVE);
		destAccount.setBalance(new BigDecimal("123"));
		
		when(acctInfo.fetchSourceAccountDetails(any(AcctTransferRequest.class))).thenReturn(srcAccount);
		
		
		when(acctInfo.fetchDestinAccountDetails(any(AcctTransferRequest.class))).thenReturn(destAccount);
		
		AcctTransferRequest request = new AcctTransferRequest();
		request.setTransferAmt("1000");
		TransferManagerInterface.FIN_VALIDN_STATUS returnStatus = testClass.validate(request);
		
		assertEquals(TransferManagerInterface.FIN_VALIDN_STATUS.SUCCESS, returnStatus);
	}

	@Test
	public void testFinancialValidatorSourceAccountValidBalanceInsufficient() {
		
		AccountInfoInterface acctInfo = mock(AccountInfoInterface.class);
		FinancialValidatorInterface testClass =  FinancialValidatorImpl.getInstance(acctInfo);
		
		Account srcAccount = new Account();
		srcAccount.setAccountStatus(Account.ACCOUNT_STATUS.ACTIVE);
		srcAccount.setBalance(new BigDecimal("12"));
	
		Account destAccount = new Account();
		destAccount.setAccountStatus(Account.ACCOUNT_STATUS.ACTIVE);
		destAccount.setBalance(new BigDecimal("123"));
		
		when(acctInfo.fetchSourceAccountDetails(any(AcctTransferRequest.class))).thenReturn(srcAccount);
		
		
		when(acctInfo.fetchDestinAccountDetails(any(AcctTransferRequest.class))).thenReturn(destAccount);
		
		AcctTransferRequest request = new AcctTransferRequest();
		request.setTransferAmt("1000");
		TransferManagerInterface.FIN_VALIDN_STATUS returnStatus = testClass.validate(request);
		
		assertEquals(TransferManagerInterface.FIN_VALIDN_STATUS.SRC_INSUFF_FUNDS, returnStatus);
	}
	
	@Test
	public void testFinancialValidatorDestinAccountInValid() {
		
		AccountInfoInterface acctInfo = mock(AccountInfoInterface.class);
		FinancialValidatorInterface testClass =  FinancialValidatorImpl.getInstance(acctInfo);
		
		Account srcAccount = new Account();
		srcAccount.setAccountStatus(Account.ACCOUNT_STATUS.ACTIVE);
		srcAccount.setBalance(new BigDecimal("1000000"));
	
		Account destAccount = new Account();
		destAccount.setAccountStatus(Account.ACCOUNT_STATUS.INACTIVE);
		destAccount.setBalance(new BigDecimal("123"));
		
		when(acctInfo.fetchSourceAccountDetails(any(AcctTransferRequest.class))).thenReturn(srcAccount);
		
		
		when(acctInfo.fetchDestinAccountDetails(any(AcctTransferRequest.class))).thenReturn(destAccount);
		
		AcctTransferRequest request = new AcctTransferRequest();
		request.setTransferAmt("1000");
		TransferManagerInterface.FIN_VALIDN_STATUS returnStatus = testClass.validate(request);
		
		assertEquals(TransferManagerInterface.FIN_VALIDN_STATUS.DESTN_ACCT_INVALID, returnStatus);
	}	
}
