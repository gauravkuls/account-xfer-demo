/*
 * (C) Copyright 2020 Gaurav K
 * 
 * 
 */
		
package com.gauravk.acctxfer.gen.util;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.gauravk.acctxfer.gen.AccountTransferNotificationSingle;
import com.gauravk.acctxfer.gen.AcctTransferNotificationRequest;
import com.gauravk.acctxfer.gen.AcctTransferRequest;
import com.gauravk.acctxfer.wire.WireTransferRequest;
import com.gauravk.acctxfer.wire.WireTransferRequest.WIRE_COUNTRY;

public class JacksonUtilTest {

	@Test
	public void testWriteObject() throws Exception{
		
		JacksonUtilInterface jUtil = JacksonUtil.getInstance();
		
		WireTransferRequest wireTransferRequest = new WireTransferRequest();
		
		wireTransferRequest.setDestinAccountNumber("#destinAccountNumber");
		wireTransferRequest.setDestinCountry("R_CTRY_C");
		
		wireTransferRequest.setWireSourceCountry(WIRE_COUNTRY.USA);
		wireTransferRequest.setWireDestinCountry(WIRE_COUNTRY.USA);
		wireTransferRequest.setDestinRoutingNumber("#routingNum");
		wireTransferRequest.setDestinAccountNumber("#destAcctNum");
		wireTransferRequest.setOriginName("Ms Source");
		wireTransferRequest.setDestinName("Ms Destin");
		wireTransferRequest.setOriginAccountNumber("#originAcctNumber");
		wireTransferRequest.setOriginRoutingNumber("#originRoutingNumber");
		wireTransferRequest.setTransferAmt("2022");
		
		jUtil.writeWire(wireTransferRequest);
		
		
	}
	
	@Test
	public void testWriteObjectAchReq() throws Exception{
		
		JacksonUtilInterface jUtil = JacksonUtil.getInstance();
		
		AcctTransferRequest achTransferRequest = new AcctTransferRequest();
		
		achTransferRequest.setDestinAccountNumber("#destinAccountNumber");
		achTransferRequest.setDestinCountry("R_CTRY_C");
		
		achTransferRequest.setDestinRoutingNumber("#routingNum");
		achTransferRequest.setDestinAccountNumber("#destAcctNum");
		achTransferRequest.setOriginName("Ms Source");
		achTransferRequest.setDestinName("Ms Destin");
		achTransferRequest.setOriginAccountNumber("#originAcctNumber");
		achTransferRequest.setOriginRoutingNumber("#originRoutingNumber");
		achTransferRequest.setTransferAmt("2022");
		
		jUtil.writeAch(achTransferRequest);
		
		
	}
	
	@Test
	public void testWriteObjectAchNotificnReq() throws Exception{
		
		JacksonUtilInterface jUtil = JacksonUtil.getInstance();
		
		AcctTransferNotificationRequest notificationTransferRequest = new AcctTransferNotificationRequest();
		
		Set<AccountTransferNotificationSingle> notifications = new HashSet<>();
		
		AccountTransferNotificationSingle e = new AccountTransferNotificationSingle();
		e.setToken("TestToken");
		e.setErrorMessage("No Error");
		e.setTransferStatus(AccountTransferNotificationSingle.TRANSFER_STATUS.COMPLETE);
		notifications.add(e );
		
		notificationTransferRequest.setNotifications(notifications);
		
		jUtil.writeAchNotificationReq(notificationTransferRequest);
		
		
	}	
}
